"""CLI main entry point."""
from argparse import ArgumentParser
import sys

import docutils.parsers.rst  # type: ignore

import dummy.__version__


def get_args_parser(description=''):  # type: (str) -> ArgumentParser
    """Add all options and arguments."""
    parser = ArgumentParser(description=description)
    parser.add_argument('path', nargs='*', help='a filename to check')
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        default=False,
        help="verbose output.",
        action="store_true")
    parser.add_argument(
        "--version",
        dest="version",
        default=False,
        help="print version and meta information",
        action="store_true")
    parser.add_argument(
        "--debug",
        dest="debug",
        default=False,
        help="debug output.",
        action="store_true")
    return parser


def evaluate_options(args):
    """Validate options, set defaults."""
    if args.version:
        print("%s: \t%s" % ('dummy', dummy.__version__.VERSION))
    return args


def main():  # Ignore VultureBear
    main_with_argv(sys.argv)


def main_with_argv(argv):  # Ignore VultureBear
    """CLI entry point."""
    args = argv[1:]
    parser = get_args_parser()  # type : ArgumentParser
    args = parser.parse_args(args)

    args = evaluate_options(args)

    print(args.path)
    for filename in args.path:
        with open(filename, 'r') as stream:
            default_settings = docutils.frontend.OptionParser(components=(
                docutils.parsers.rst.Parser, )).get_default_values()
            document = docutils.utils.new_document(filename, default_settings)
            docparser = docutils.parsers.rst.Parser()
            docparser.parse(stream.read(), document)
            print(document)
    return 0
