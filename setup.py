"""setup.py. Most of the configuration moved to setup.cfg."""
from setuptools import setup

from buildsrc.clean import CleanCommand
from buildsrc.typecheck import TypecheckCommand

# most settings in setup.cfg
setup(
    # packages needed for production code
    # sadly currently not referencable from setup.cfg
    install_requires=open('requirements/main.txt').read().splitlines(),
    # packages auto-provided on "python setup.py test"
    # sadly currently not referencable from setup.cfg
    tests_require=open('requirements/test.txt').read().splitlines(),
    # markdown also works with long_descript_mediatype
    cmdclass={
        'typecheck': TypecheckCommand,
        'clean': CleanCommand,
    },
)
