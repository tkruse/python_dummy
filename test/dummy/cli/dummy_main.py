"""Main test."""

from dummy.cli import dummy_main

try:
    from mock import MagicMock
except ImportError:
    from unittest.mock import MagicMock  # type: ignore


def test_get_args_parser():
    assert dummy_main.get_args_parser() is not None


def test_evaluate_options():
    options_mock = MagicMock()
    options_mock.version = False
    assert dummy_main.evaluate_options(options_mock) == options_mock
    options_mock.version = True
    assert dummy_main.evaluate_options(options_mock) == options_mock


def test_main():
    assert dummy_main.main_with_argv(['', 'README.rst']) == 0
