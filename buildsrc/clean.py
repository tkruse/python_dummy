import fnmatch
import os
import shutil

from setuptools import Command


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []  # type: list
    description = 'deletes all temp folders'

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):  # pylint: disable=I0011,R0201
        for folder in './build ./dist ./.eggs ./.tox'.split(' '):
            self.delete(folder)
        for glob in '__pycache__ ./*.pyc ./*.tgz ./*.egg-info ./*.orig ./*~'.split(' '):
            print("cleaning " + glob)
            for _, dirnames, filenames in os.walk('src'):
                for dirname in fnmatch.filter(dirnames, glob):
                    self.delete(str(dirname))
                for filename in fnmatch.filter(filenames, glob):
                    self.delete(str(filename))

    @staticmethod
    def delete(filename):
        print("deleting %s" % filename)
        if os.path.isfile(filename):
            os.remove(filename)
        else:
            shutil.rmtree(filename, ignore_errors=True)
