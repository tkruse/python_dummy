"""parent of other commands"""
# pylint: disable=I0011,E0401,E0611
from distutils.spawn import find_executable
from subprocess import CalledProcessError
from subprocess import check_call
import sys

from setuptools import Command


class BaseCommand(Command):
    user_options = [(
        'executable', 'e', 'The executable to use for the command'
    )]
    executable = ''

    def initialize_options(self):
        resolved = find_executable(self.executable)
        if resolved is None:
            raise ValueError("Cannot find executable named " + self.executable)
        self.executable = resolved

    def finalize_options(self):
        pass

    def execute(self, *k):
        try:
            cmd = (self.executable,) + k
            check_call(cmd)
        except CalledProcessError:
            sys.exit(1)
