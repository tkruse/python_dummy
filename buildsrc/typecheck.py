from buildsrc.commands import BaseCommand


class TypecheckCommand(BaseCommand):
    """command to be used in setup.py"""
    description = 'Run mypy over the code'
    user_options = BaseCommand.user_options + [
        ('ignore-missing-imports', None,  # type: ignore
         'Do not warn for missing imports'),
        ('py2', '2', 'Allow python2 code')
    ]
    executable = 'mypy'

    def run(self):
        # for now, require source files to be python2 compatible
        self.execute('--py2')
