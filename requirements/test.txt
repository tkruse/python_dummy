# dependencies needed to run tests
pytest
mock
coverage
flake8
pytest-cov
pytest-pylint
pytest-flake8
pytest-gitignore
