Python Dummy Project
====================

.. image:: https://travis-ci.org/tkruse/python_dummy.svg?branch=master
    :target: https://travis-ci.org/tkruse/python_dummy

This project demonstrates how to set up a python2/3 project with exhaustive usage of QA tools.

It is generally preferable to use one of the scaffolding tools here:

* https://yehua.readthedocs.io/en/latest/

Other resources:

* http://andrewsforge.com/article/python-new-package-landscape/
* https://dan.yeaw.me/posts/python-packaging-with-poetry-and-briefcase/
* https://sourcery.ai/blog/python-best-practices/
* https://www.pydanny.com/python-dot-py-tricks.html
* https://www.lutro.me/posts/better-pip-dependency-management
* https://jacobian.org/2019/nov/11/python-environment-2020/

It can be used as a template for new projects of similar nature (CLI tools, libraries).

This structure evolves over time and may not be working or optimal for whichever tool versions are being used.

Features
--------

* Most tool configuration done in `setup.cfg`, not `setup.py`
* Most Linters configured via `coala` [https://coala.io/], see `.coafile`

  * pep8
  * pydocstyle
  * pyflakes
  * pylint
  * bandit
  * isort
  * radon
  * vulture
  * yapf

* Testing done by `pytest` [https://docs.pytest.org], not nose
* MyPy type checks (not done using pytest-mypy, because version 0.4.2 has too many issues configuring it)
* Multi-env testing capabilities by `tox` [https://tox.readthedocs.io], see `tox.ini`
* setup.cfg aliases for all build tasks (except tox, which calls setup.cfg)
* All commands used from tox can easily be run locally without tox (See CONTRIBUTING.rst)
* All checks can be run via setup.py commands
* CI configuration for travis

Prerequisites
-------------

Python 2 or 3

   
Motivation
----------

Over the years the Python ecosystem has accumulated a confusing number of different tools for development, each having their own cli options, config files, config file syntax. Also many of them having overlapping features.

This makes it hard to make good decisions when setting up new projects.

For this dummy, the following ideas were followed:

* Configuration should ideally not be duplicated (in particular dependency versions)
  This is not perfectly possible while staying Python2/Python3 compatible
* The individual commands of each wrapping tool should easily be callable individually
* source in `src` folder
* Put as little logic as possible into `.travis.yml`.
  While travis offers powerful features for setup and matrix build, using them makes it hard to reproduce problems locally, so tox is a much better tool for that.
* Put as little logic as possible into `tox.ini`
  While tox offers powerful features for setup and matrix build, using them makes it hard to reproduce problems without tox, and tox is slow and cumbersome.
* Put as much linter configuration into `.coafile` as possible.
  This seems to be the only sane way to deal with the flood of linters available.
  Sadly when trying to remain Python2/Python3 compatible, a world of pain still waits for the unfortunate.
* Avoid other popular tools such as nose, pylama, prospector, invoke, ... Pytest, Coala and tox seem to be the current stars (coala slowly fading...?)
* Avoid Makefile to define build commands

TODO
----

* Decide whether to run pylint standalone, with pytest, or with coala...
* Decide on pylint vs. flake8
* Decide whether to dump coala because it becomes inactive

* moban
* flit
* pipenv
* pipx
* poetry
* black
* isort
* Provide as cookiecutter template
* Python3 only version with new mypy syntax
* Appveyor CI integration for Windows compatibility
* codeclimate
* codecov
